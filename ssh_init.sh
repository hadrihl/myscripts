#!/bin/sh
iptables -A INPUT -p tcp -s 0/0 -d 10.207.146.60 --sport 513:65535 --dport 22 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -p tcp -s 10.207.146.60 -d 0/0 --sport 22 --dport 513:65535 -m state --state ESTABLISHED -j ACCEPT
iptables -L -v

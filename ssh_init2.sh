#!/bin/sh
iptables -A INPUT -p tcp --dport 22 -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -m state --state NEW -m limit --limit 2/min --limit-burst 2 -j ACCEPT
iptables -A INPUT -p tcp --dport 22 -j DROP

# View
iptables -L -v
